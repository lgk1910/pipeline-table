import argparse
from functools import partial
import os
import sys

from PIL import Image
import numpy as np

import tritonclient.grpc as grpcclient
import tritonclient.grpc.model_config_pb2 as mc

import tritonclient.http as httpclient
from tritonclient.utils import InferenceServerException
from tritonclient.utils import triton_to_np_dtype
import time

class TableLineServing:
    def __init__(self, model_name='serving', url='127.0.0.1:8001'):
        self.model_name = model_name
        self.triton_client = grpcclient.InferenceServerClient(
            url=url, verbose=False)
        self.model_metadata = self.triton_client.get_model_metadata(
            model_name=model_name, model_version='1')
        model_config = self.triton_client.get_model_config(
            model_name=model_name, model_version='1')
        model_config = model_config.config
        self.max_batch_size, self.input_name, self.output_name, self.format, self.dtype = self.parse_model(
            self.model_metadata, model_config)

    def parse_model(self, model_metadata, model_config):
        """
        Check the configuration of a model to make sure it meets the
        requirements for an image classification network (as expected by
        this client)
        """
        if len(model_metadata.inputs) != 1:
            raise Exception("expecting 1 input, got {}".format(
                len(model_metadata.inputs)))
        if len(model_metadata.outputs) != 1:
            raise Exception("expecting 1 output, got {}".format(
                len(model_metadata.outputs)))

        if len(model_config.input) != 1:
            raise Exception(
                "expecting 1 input in model configuration, got {}".format(
                    len(model_config.input)))

        input_metadata = model_metadata.inputs[0]
        input_config = model_config.input[0]
        output_metadata = model_metadata.outputs
        output_names = []
        for metadata in output_metadata:
            output_names.append(metadata.name)
            # print(metadata.shape)

        for metadata in output_metadata:
            if metadata.datatype != "FP32":
                raise Exception("expecting output datatype to be FP32, model '" +
                                model_metadata.name + "' output type is " +
                                metadata.datatype)

        return (model_config.max_batch_size, input_metadata.name,
                output_names, input_config.format,
                input_metadata.datatype)

    def requestGenerator(self, batched_image_data, model_name='serving', model_version='1', input_name='input',
                         output_names=['output'], dtype='FP32'):
        protocol = 'grpc'

        if protocol == "grpc":
            client = grpcclient
        else:
            client = httpclient

        # Set the input data
        if dtype == 'FP32':
            batched_image_data = batched_image_data.astype(dtype=np.float32, copy=False)

        inputs = [client.InferInput(input_name, batched_image_data.shape, dtype)]

        inputs[0].set_data_from_numpy(batched_image_data)

        outputs = [
            client.InferRequestedOutput(output) for output in output_names
            # client.InferRequestedOutput('284__0')
        ]
        # print(f'output names: {output_names}')
        return inputs, outputs, model_name, model_version

    def preprocess(self, imgs, format, dtype, protocol):
        """
        Pre-process an image to meet the size, type and format
        requirements specified by the parameters.
        """

        npdtype = triton_to_np_dtype(dtype)
        typed = imgs.astype(npdtype)

        # Channels are in RGB order. Currently model configuration data
        # doesn't provide any information as to other channel orderings
        # (like BGR) so we just assume RGB.
        return typed

    def run_inference_grpc(self, imgs, model_version='1'):
        imgs = self.preprocess(imgs, format, self.dtype, 'grpc')

        streaming = False
        protocol = 'grpc'

        responses = []
        batch_size = imgs.shape[0]
        sent_count = 0

        inputs, outputs, model_name, model_version = self.requestGenerator(batched_image_data=imgs,
                                                                           model_name=self.model_name,
                                                                           input_name=self.input_name,
                                                                           output_names=self.output_name, dtype=self.dtype)
        try:
            responses.append(
                self.triton_client.infer(model_name,
                                    inputs,
                                    request_id=str(sent_count),
                                    model_version=model_version,
                                    outputs=outputs))

        except InferenceServerException as e:
            print("inference failed: " + str(e))
            if streaming:
                self.triton_client.stop_stream()
            sys.exit(1)

        for response in responses:
            if protocol.lower() == "grpc":
                this_id = response.get_response().id
            else:
                this_id = response.get_response()["id"]
            # print("Request {}, batch size {}".format(this_id, batch_size))
            output = response.as_numpy('conv2d_32')
            # print(f'output shape: {output.shape}')
            return output

if __name__ == '__main__':
    imgs = np.zeros((1, 1024, 1024, 3))
    table_line_serving = TableLineServing(model_name='table_line', url='localhost:8001')
    # gRPC inference
    start = time.time()
    # change the url whenever needed, default is localhost:8001
    output = table_line_serving.run_inference_grpc(imgs=imgs)
    print(f'output shape: {output.shape}')
    print(f'expected output shape: {(imgs.shape[0], imgs.shape[1], imgs.shape[2], 2)}')
    print(f"gRPC inference time: {time.time() - start}s")