import cv2
import numpy as np
import sys
from table_models import table_net
from tablenet_utils import minAreaRectbox, measure, draw_lines,draw_boxes_v2,letterbox_image, get_table_line, adjust_lines, line_to_line,draw_boxes,minAreaRectbox_v2,Sortlayer
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow as tf
# from table_line.triton_client import TableLineServing
from glob import glob
# import os

# gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.1)
# sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(gpu_options=gpu_options))

class tablenet:
    '''
    Class này sử dụng cho dạng full-border
    Model hiện tại đang sử dụng là kiến trúc mạng Unet
    refers repo: https://github.com/chineseocr/table-ocr
    @model: model_line
    @img: ảnh đầu vào
    @size: kích thước đầu vào
    @engine: core ocr <easyocr|akaocr>
    @reader: ocr 
    '''
    def __init__(self,model,engine,reader,size = (1024,1024), method='pth'):
        if method == 'pth':
            self.model = model
        elif method == 'triton':
            self.model = TableLineServing(model_name='table_line', url='localhost:8001') 
        self.size = size
        self.engine = engine
        self.reader = reader
        self.method = method

    def infer(self,img):
        #processing img
        sizew, sizeh = self.size
        inputBlob, fx, fy = letterbox_image(img[..., ::-1], (sizew, sizeh))
        hprob=0.5 #0.5
        vprob=0.5 #0.5
        # print(f'input shape: {np.array([np.array(inputBlob) / 255.0]).shape}')
        if self.method == 'pth':
            pred = self.model.predict(np.expand_dims(np.array(inputBlob) / 255.0, axis=0))
        elif self.method == 'triton':
            pred = self.model.run_inference_grpc(imgs=np.expand_dims(np.array(inputBlob) / 255.0, axis=0))

        # print(f'output shape: {pred.shape}')
        #rows = 0, cols = 1
        pred = pred[0]
        vpred = pred[..., 1] > vprob 
        hpred = pred[..., 0] > hprob 
        vpred = vpred.astype(int)
        hpred = hpred.astype(int)

        #xử lý các đường line sau predict
        row=50 #100
        col=50 #100
        colboxes = get_table_line(vpred, axis=1, lineW=col)
        rowboxes = get_table_line(hpred, axis=0, lineW=row)
        ccolbox = []
        crowlbox = []
        if len(rowboxes) > 0:
                rowboxes = np.array(rowboxes)
                rowboxes[:, [0, 2]] = rowboxes[:, [0, 2]] / fx
                rowboxes[:, [1, 3]] = rowboxes[:, [1, 3]] / fy
                xmin = rowboxes[:, [0, 2]].min()
                xmax = rowboxes[:, [0, 2]].max()
                ymin = rowboxes[:, [1, 3]].min()
                ymax = rowboxes[:, [1, 3]].max()
                ccolbox = [[xmin, ymin, xmin, ymax], [xmax, ymin, xmax, ymax]]
                rowboxes = rowboxes.tolist()

        if len(colboxes) > 0:
                colboxes = np.array(colboxes)
                colboxes[:, [0, 2]] = colboxes[:, [0, 2]] / fx
                colboxes[:, [1, 3]] = colboxes[:, [1, 3]] / fy

                xmin = colboxes[:, [0, 2]].min()
                xmax = colboxes[:, [0, 2]].max()
                ymin = colboxes[:, [1, 3]].min()
                ymax = colboxes[:, [1, 3]].max()
                colboxes = colboxes.tolist()
                crowlbox = [[xmin, ymin, xmax, ymin], [xmin, ymax, xmax, ymax]]
        rowboxes += crowlbox
        colboxes += ccolbox

        alph = 3
        rboxes_row_, rboxes_col_ = adjust_lines(rowboxes, colboxes, alph=alph)
        rowboxes += rboxes_row_
        colboxes += rboxes_col_

        nrow = len(rowboxes)
        ncol = len(colboxes)
        for i in range(nrow):
                for j in range(ncol):
                    rowboxes[i] = line_to_line(rowboxes[i], colboxes[j], 500)
                    colboxes[j] = line_to_line(colboxes[j], rowboxes[i], 500)

        tmp = np.zeros(img.shape[:2], dtype='uint8')
        tmp = draw_lines(tmp, rowboxes + colboxes, color=255, lineW=4)
        labels = measure.label(tmp < 255, connectivity=2)  
        regions = measure.regionprops(labels)
        ceilboxes = minAreaRectbox(regions, False, tmp.shape[1], tmp.shape[0], True, True)
        boxes = minAreaRectbox_v2(regions, False, tmp.shape[1], tmp.shape[0], True, True)
        ceilboxes = np.array(ceilboxes)
        img_,_= draw_boxes_v2(img,ceilboxes)
        
        return ceilboxes,boxes,img_

        return ceilboxes,boxes

    def ocr(self,img):
        # #infer table lines
        # ceilboxes,boxes = self.infer(img)
        # sortbbox = Sortlayer()
        # ceilboxes,rows = sortbbox(boxes)

        # outer = []
        # ls = []

        # #infer character on whole page
        # outs = self.reader.detect_and_recognize(img)
        # list_outs = list(outs)

        # #loop and match
        # for box in ceilboxes:
        #     x1_, y1_, x2_, y2_, x3_, y3_, x4_, y4_ = box[0][0],box[0][1],box[1][0],box[1][1],box[2][0],box[2][1],box[3][0],box[3][1]
        #     x1, y1, x2, y2, x3, y3, x4, y4 = int(x1_),int(y1_),int(x2_),int(y2_),int(x3_),int(y3_),int(x4_),int(y4_)
        #     top_left_x = min([x1,x2,x3,x4])
        #     top_left_y = min([y1,y2,y3,y4])
        #     bot_right_x = max([x1,x2,x3,x4])
        #     bot_right_y = max([y1,y2,y3,y4])
            
        #     sub_img = img[top_left_y:bot_right_y, top_left_x:bot_right_x]
        #     new_w,new_h,c = sub_img.shape
        
        #     if new_h > 30 and new_w >=16:
        #         ls.append([[x1_, y1_],[ x2_, y2_],[x3_, y3_],[x4_, y4_]])
        #         sub_word = []
        #         for result in list_outs:
        #             x1,y1,x2,y2 = result[0]
        #             x_center, y_center = (x1+x2)/2, (y1+y2)/2
        #             # print("Text:", result[1])
        #             if x_center > top_left_x and y_center > top_left_y and x_center < bot_right_x and y_center < bot_right_y:
        #                 sub_word.append(result[1])
        #         out = ''
        #         for i, result in enumerate(sub_word):
        #             if i == 0:
        #                 out += result
        #             else:
        #                 out += ' ' + result
        #         outer.append(out)
        #         print("Out:", out)

        # # dataframe = to_csv(outer,rows,ls)
        # arr = np.array(outer)

        # count = 0
        # for i in rows[0]:
        #     if i in ls:
        #         count +=1

        # try:
        #     arr = arr.reshape(len(arr)//count,count)
        #     # dataframe = pd.DataFrame(arr)
        #     # dataframe.to_csv('/home/ec2-user/pipeline_table_extraction/output/csv/test1.csv', index=False)
        #     return arr
        # except:
        #     print('ERROR: CAN NOT RESHAPE TO TABLE!!!')
        #     return arr

        #------------------------------------------------------------------------------------
        #infer table line
        ceilboxes_org,boxes,img = self.infer(img)
        #sắp xếp lại bbox của từng ceil
        sortbbox = Sortlayer()
        ceilboxes,rows = sortbbox(boxes)

        outer = []
        ls = []
        for box in ceilboxes:
            x1_, y1_, x2_, y2_, x3_, y3_, x4_, y4_ = box[0][0],box[0][1],box[1][0],box[1][1],box[2][0],box[2][1],box[3][0],box[3][1]
            x1, y1, x2, y2, x3, y3, x4, y4 = int(x1_),int(y1_),int(x2_),int(y2_),int(x3_),int(y3_),int(x4_),int(y4_)
            top_left_x = min([x1,x2,x3,x4])
            top_left_y = min([y1,y2,y3,y4])
            bot_right_x = max([x1,x2,x3,x4])
            bot_right_y = max([y1,y2,y3,y4])
            sub_img = img[top_left_y:bot_right_y, top_left_x:bot_right_x]
            new_w,new_h,c = sub_img.shape
            if new_h > 30 and new_w >=16:
                ls.append([[x1_,y1_],[x2_,y2_],[x3_,y3_],[x4_,y4_]])
                if self.engine == 'akaocr':
                    results = self.reader.detect_and_recognize(sub_img)
                    outer.append(results[0])
                elif self.engine == 'easyocr':
                    results = self.reader.readtext(sub_img)
                    plain_text = ""
                    for box in results:
                        plain_text += box[-2]
                        plain_text += " "
                    # print(plain_text)
                    outer.append(plain_text)

        arr = np.array(outer) 

        count = 0
        for i in rows[0]:
            if i in ls:
                count +=1

        try:
            arr = arr.reshape(len(arr)//count,count)
            # dataframe = pd.DataFrame(arr)
            # dataframe.to_csv('/home/ec2-user/pipeline_table_extraction/output/csv/test1.csv', index=False)
            return arr, ls
        except:
            print('ERROR: CAN NOT RESHAPE TO TABLE!!!')
            return arr, ls


