import os
import json
from datetime import datetime
import string
import sys
import random
import numpy as np
import cv2
import glob
from tqdm import tqdm
import sys
import pandas as pd

sys.path.append("../tableTrans/data")
sys.path.append("../tableTrans/source")
sys.path.append("../tablenet")

# from tablenet import tablenet
from table_models import table_net
from tablenet_utils import minAreaRectbox, measure, draw_lines,draw_boxes_v2,letterbox_image, get_table_line, adjust_lines, line_to_line,draw_boxes,minAreaRectbox_v2,Sortlayer
import easyocr

import torch
from torchvision.transforms import functional as F
from engine import evaluate, train_one_epoch
import data.transforms as R

# New
from data.loader import get_data
from modeling.model import get_model
import utils.misc as utils
from utils.early_stopping import EarlyStopper
from utility import get_args, BoundingBox

class TableTransformer:
    def __init__(self, det_args, device, class_map, model_type='det'):
        model, criterion, postprocessors = get_model(det_args, device)
        self.model = model
        self.postprocessors = postprocessors
        self.device = device
        self.class_map = class_map
        self.model_type = model_type
        
    def rescale_img(self, img, fraction=2):
        width = int(img.shape[1] * fraction)
        height = int(img.shape[0] * fraction)
        dim = (width, height)
        
        # resize image
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        return resized

    def infer(self, args, image, output_filename='output'):
        output_dir = args.output_dir
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
            
        normalize = R.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        h, w, _ = image.shape
        print(f'w, h: {w} {h}')
        fraction = 1
        img = image
        if (w > h and w > 3000 and w < 5000) or (h > w and h > 3000 and h < 5000):
            fraction = 0.33
            img = self.rescale_img(image, fraction)
            print(f'new img shape {img.shape}')
        elif (w > h and w > 5000 and w < 7000) or (h > w and h > 5000 and h < 7000):
            fraction = 0.2
            img = self.rescale_img(image, fraction)
            print(f'new img shape {img.shape}')
        elif (w >= 7000) or (h >= 7000):
            fraction = 0.15
            img = self.rescale_img(image, fraction)
            print(f'new img shape {img.shape}') 
        else:
            img = image
                
        img_tensor = normalize(F.to_tensor(img))[0]
        img_tensor = torch.unsqueeze(img_tensor, 0).to(self.device)

        outputs = None
        with torch.no_grad():
            outputs = self.model(img_tensor)

        image_size = torch.unsqueeze(torch.as_tensor([int(h), int(w)]), 0).to(self.device)
        results = self.postprocessors["bbox"](outputs, image_size)[0]
        # print(results)
        class_map = get_class_map()
        ltrb_label = ''
        predictions = []
        if args.debug is True:
            for idx, score in enumerate(results["scores"].tolist()):
                if score < args.thresh:
                    continue

                xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
                
                # Padding table
                if self.model_type == 'det':
                    xmin = max(xmin - 30, 0)
                    xmax = min(xmax + 30, img.shape[1])
                    ymin = max(ymin - 30, 0)
                    ymax = min(ymax + 30, img.shape[0])
                    
                predictions.append({
                    'bbs': [xmin, ymin, xmax, ymax],
                    'label': class_map[results["labels"][idx].item()],
                    'score': score 
                })
                ltrb_label += f'{class_map[results["labels"][idx].item()]} {score} {int(xmin/fraction)} {int(ymin/fraction)} {int(xmax/fraction)} {int(ymax/fraction)}\n'
                cv2.rectangle(image, (xmin, ymin), (xmax, ymax), (0, 255, 0), 2)
            results["debug_image"] = image
            with open(os.path.join(output_dir, f'{output_filename}.txt'), 'w') as f:
                f.write(ltrb_label)
            cv2.imwrite(os.path.join(output_dir, f'{output_filename}.jpg'), image)
        else:
            for idx, score in enumerate(results["scores"].tolist()):
                if score < args.thresh:
                    continue
                xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
                predictions.append({
                    'bbs': [xmin, ymin, xmax, ymax],
                    'label': class_map[results["labels"][idx].item()],
                    'score': score
                })
                ltrb_label += f'{class_map[results["labels"][idx].item()]} {score} {int(xmin/fraction)} {int(ymin/fraction)} {int(xmax/fraction)} {int(ymax/fraction)}\n'
            with open(os.path.join(output_dir, 'output_det.txt'), 'w') as f:
                f.write(ltrb_label)
        print(f'Infer successfully, output saved at {output_dir}')
        return predictions

class TableNet:
    '''
    Class này sử dụng cho dạng full-border
    Model hiện tại đang sử dụng là kiến trúc mạng Unet
    refers repo: https://github.com/chineseocr/table-ocr
    @model: model_line
    @img: ảnh đầu vào
    @size: kích thước đầu vào
    @engine: core ocr <easyocr|akaocr>
    @reader: ocr 
    '''
    def __init__(self, model_weights, engine, reader, size = (1024,1024)):
        model = table_net((None, None, 3), 2)
        model.load_weights(model_weights)
        self.model = model
        self.size = size
        self.engine = engine
        self.reader = reader

    def infer(self, img):
        #processing img
        sizew, sizeh = self.size
        inputBlob, fx, fy = letterbox_image(img[..., ::-1], (sizew, sizeh))
        hprob=0.5 #0.5
        vprob=0.5 #0.5
        # print(f'input shape: {np.array([np.array(inputBlob) / 255.0]).shape}')
        pred = self.model.predict(np.expand_dims(np.array(inputBlob) / 255.0, axis=0))
        # print(f'output shape: {pred.shape}')
        #rows = 0, cols = 1
        pred = pred[0]
        vpred = pred[..., 1] > vprob 
        hpred = pred[..., 0] > hprob 
        vpred = vpred.astype(int)
        hpred = hpred.astype(int)

        #xử lý các đường line sau predict
        row=50 #100
        col=50 #100
        colboxes = get_table_line(vpred, axis=1, lineW=col)
        rowboxes = get_table_line(hpred, axis=0, lineW=row)
        ccolbox = []
        crowlbox = []
        if len(rowboxes) > 0:
            rowboxes = np.array(rowboxes)
            rowboxes[:, [0, 2]] = rowboxes[:, [0, 2]] / fx
            rowboxes[:, [1, 3]] = rowboxes[:, [1, 3]] / fy
            xmin = rowboxes[:, [0, 2]].min()
            xmax = rowboxes[:, [0, 2]].max()
            ymin = rowboxes[:, [1, 3]].min()
            ymax = rowboxes[:, [1, 3]].max()
            ccolbox = [[xmin, ymin, xmin, ymax], [xmax, ymin, xmax, ymax]]
            rowboxes = rowboxes.tolist()

        if len(colboxes) > 0:
            colboxes = np.array(colboxes)
            colboxes[:, [0, 2]] = colboxes[:, [0, 2]] / fx
            colboxes[:, [1, 3]] = colboxes[:, [1, 3]] / fy

            xmin = colboxes[:, [0, 2]].min()
            xmax = colboxes[:, [0, 2]].max()
            ymin = colboxes[:, [1, 3]].min()
            ymax = colboxes[:, [1, 3]].max()
            colboxes = colboxes.tolist()
            crowlbox = [[xmin, ymin, xmax, ymin], [xmin, ymax, xmax, ymax]]
        rowboxes += crowlbox
        colboxes += ccolbox

        alph = 3
        rboxes_row_, rboxes_col_ = adjust_lines(rowboxes, colboxes, alph=alph)
        rowboxes += rboxes_row_
        colboxes += rboxes_col_

        nrow = len(rowboxes)
        ncol = len(colboxes)
        for i in range(nrow):
                for j in range(ncol):
                    rowboxes[i] = line_to_line(rowboxes[i], colboxes[j], 500)
                    colboxes[j] = line_to_line(colboxes[j], rowboxes[i], 500)

        tmp = np.zeros(img.shape[:2], dtype='uint8')
        tmp = draw_lines(tmp, rowboxes + colboxes, color=255, lineW=4)
        labels = measure.label(tmp < 255, connectivity=2)  
        regions = measure.regionprops(labels)
        ceilboxes = minAreaRectbox(regions, False, tmp.shape[1], tmp.shape[0], True, True)
        boxes = minAreaRectbox_v2(regions, False, tmp.shape[1], tmp.shape[0], True, True)
        ceilboxes = np.array(ceilboxes)
        # img_,_= draw_boxes_v2(img,ceilboxes)
        return ceilboxes,boxes,img

        return ceilboxes,boxes

    def ocr(self,boxes,img):
        # #infer table lines
        # ceilboxes,boxes = self.infer(img)
        # sortbbox = Sortlayer()
        # ceilboxes,rows = sortbbox(boxes)

        # outer = []
        # ls = []

        # #infer character on whole page
        # outs = self.reader.detect_and_recognize(img)
        # list_outs = list(outs)

        # #loop and match
        # for box in ceilboxes:
        #     x1_, y1_, x2_, y2_, x3_, y3_, x4_, y4_ = box[0][0],box[0][1],box[1][0],box[1][1],box[2][0],box[2][1],box[3][0],box[3][1]
        #     x1, y1, x2, y2, x3, y3, x4, y4 = int(x1_),int(y1_),int(x2_),int(y2_),int(x3_),int(y3_),int(x4_),int(y4_)
        #     top_left_x = min([x1,x2,x3,x4])
        #     top_left_y = min([y1,y2,y3,y4])
        #     bot_right_x = max([x1,x2,x3,x4])
        #     bot_right_y = max([y1,y2,y3,y4])
            
        #     sub_img = img[top_left_y:bot_right_y, top_left_x:bot_right_x]
        #     new_w,new_h,c = sub_img.shape
        
        #     if new_h > 30 and new_w >=16:
        #         ls.append([[x1_, y1_],[ x2_, y2_],[x3_, y3_],[x4_, y4_]])
        #         sub_word = []
        #         for result in list_outs:
        #             x1,y1,x2,y2 = result[0]
        #             x_center, y_center = (x1+x2)/2, (y1+y2)/2
        #             # print("Text:", result[1])
        #             if x_center > top_left_x and y_center > top_left_y and x_center < bot_right_x and y_center < bot_right_y:
        #                 sub_word.append(result[1])
        #         out = ''
        #         for i, result in enumerate(sub_word):
        #             if i == 0:
        #                 out += result
        #             else:
        #                 out += ' ' + result
        #         outer.append(out)
        #         print("Out:", out)

        # # dataframe = to_csv(outer,rows,ls)
        # arr = np.array(outer)

        # count = 0
        # for i in rows[0]:
        #     if i in ls:
        #         count +=1

        # try:
        #     arr = arr.reshape(len(arr)//count,count)
        #     # dataframe = pd.DataFrame(arr)
        #     # dataframe.to_csv('/home/ec2-user/pipeline_table_extraction/output/csv/test1.csv', index=False)
        #     return arr
        # except:
        #     print('ERROR: CAN NOT RESHAPE TO TABLE!!!')
        #     return arr

        #------------------------------------------------------------------------------------
        #sắp xếp lại bbox của từng ceil
        sortbbox = Sortlayer()
        ceilboxes,rows = sortbbox(boxes)

        outer = []
        ls = []
        for idx, box in enumerate(ceilboxes):
            x1_, y1_, x2_, y2_, x3_, y3_, x4_, y4_ = box[0][0],box[0][1],box[1][0],box[1][1],box[2][0],box[2][1],box[3][0],box[3][1]
            x1, y1, x2, y2, x3, y3, x4, y4 = int(x1_),int(y1_),int(x2_),int(y2_),int(x3_),int(y3_),int(x4_),int(y4_)
            top_left_x = min([x1,x2,x3,x4])
            top_left_y = min([y1,y2,y3,y4])
            bot_right_x = max([x1,x2,x3,x4])
            bot_right_y = max([y1,y2,y3,y4])
            sub_img = img[top_left_y:bot_right_y, top_left_x:bot_right_x]
            new_w,new_h,c = sub_img.shape
            if new_h > 30 and new_w >=16:
                ls.append([[x1_,y1_],[x2_,y2_],[x3_,y3_],[x4_,y4_]])
                cv2.imwrite(f'demo/tablenet_cell_{idx}.jpg', sub_img)
                results = self.reader.readtext(sub_img)
                plain_text = ""
                for box in results:
                    plain_text += box[-2]
                    plain_text += " "
                # print(plain_text)
                outer.append(plain_text)

        arr = np.array(outer) 

        count = 0
        for i in rows[0]:
            if i in ls:
                count +=1

        try:
            arr = arr.reshape(len(arr)//count,count)
            # dataframe = pd.DataFrame(arr)
            # dataframe.to_csv('/home/ec2-user/pipeline_table_extraction/output/csv/test1.csv', index=False)
            return arr, ls
        except:
            print('ERROR: CAN NOT RESHAPE TO TABLE!!!')
            return arr, ls
  
class TablePipeline:
    def __init__(self, det_model, rec_model, tablenet, reader):
        self.det_model = det_model
        self.rec_model = rec_model
        self.tablenet = tablenet
        self.reader = reader
    
    def rescale_img(self, img, fraction=2):
        width = int(img.shape[1] * fraction)
        height = int(img.shape[0] * fraction)
        dim = (width, height)
        
        # resize image
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        return resized
    
    def infer_bordered_table(self, args):
        image_path = args.image_path
        image = cv2.imread(image_path)
        image_copy = np.copy(image)
        h, w, _ = image.shape
        fraction = 1
        
        if (w > h and w > 3000 and w < 5000) or (h > w and h > 3000 and h < 5000):
            fraction = 0.33
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}')
        elif (w > h and w > 5000 and w < 7000) or (h > w and h > 5000 and h < 7000):
            fraction = 0.2
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}')
        elif (w >= 7000) or (h >= 7000):
            fraction = 0.15
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}') 
            
        basename = os.path.basename(image_path)
        filename = '.'.join(basename.split('.')[:-1])
        det_predictions = self.det_model.infer(args, image, f'{filename}_det')
        tsr_predictions = []
        table_images = []
        for idx, det_prediction in enumerate(det_predictions):
            xmin, ymin, xmax, ymax = det_prediction['bbs']
            xmin = int(xmin/fraction)
            ymin = int(ymin/fraction)
            xmax = int(xmax/fraction)
            ymax = int(ymax/fraction)
            table_img = image_copy[ymin:ymax, xmin:xmax, :]
            table_img_copy = np.copy(table_img)
            ceilboxes, boxes, img_  = self.tablenet.infer(table_img_copy)
            cv2.imwrite(os.path.join(args.output_dir, f'{filename}_table_{idx}.jpg'), img_)
            print('tsr inferred successfully. Start OCR')
            arr, ls = self.tablenet.ocr(boxes, img_)
            dataframe = pd.DataFrame(arr)
            dataframe.to_excel(os.path.join(args.output_dir, f'{filename}_table_{idx}.xlsx'))
            print(dataframe)
            
    def infer(self, args):
        image_path = args.image_path
        image = cv2.imread(image_path)
        image_copy = np.copy(image)
        h, w, _ = image.shape
        fraction = 1
        
        if (w > h and w > 3000 and w < 5000) or (h > w and h > 3000 and h < 5000):
            fraction = 0.33
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}')
        elif (w > h and w > 5000 and w < 7000) or (h > w and h > 5000 and h < 7000):
            fraction = 0.2
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}')
        elif (w >= 7000) or (h >= 7000):
            fraction = 0.15
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}') 
            
        basename = os.path.basename(image_path)
        filename = '.'.join(basename.split('.')[:-1])
        det_predictions = self.det_model.infer(args, image, f'{filename}_det')
        tsr_predictions = []
        table_images = []
        for idx, det_prediction in enumerate(det_predictions):
            xmin, ymin, xmax, ymax = det_prediction['bbs']
            xmin = int(xmin/fraction)
            ymin = int(ymin/fraction)
            xmax = int(xmax/fraction)
            ymax = int(ymax/fraction)
            table_img = image_copy[ymin:ymax, xmin:xmax, :]
            table_img_copy = np.copy(table_img)
            predictions = self.rec_model.infer(args, table_img_copy, f'{filename}_table_{idx}')
            tsr_predictions.append(predictions)
            table_images.append(table_img)
        
        for idx, tsr_prediction, table_image in zip(range(len(table_images)), tsr_predictions, table_images):
            grid = self.pred2df(tsr_prediction, table_image)
            df = pd.DataFrame(grid)
            df.to_csv(os.path.join(args.output_dir, f'{filename}_table_{idx}.csv'), index=False)
            df.to_excel(os.path.join(args.output_dir, f'{filename}_table_{idx}.xlsx'))
        print('Pipeline run successfully')
        return
    
    def pred2df(self, tsr_prediction, image):
        rows = []
        cols = []
        spanning_cells = []
        for entity in tsr_prediction:
            if entity['label'] in ['table row', 'column header']:
                rows.append({
                    'bbs': entity['bbs'],
                    'type': entity['label']
                })
            elif entity['label'] in ['table column']:
                cols.append({
                    'bbs': entity['bbs'],
                    'type': entity['label']
                })
            elif entity['label'] in ['table spanning cell', 'table projected row header']:
                spanning_cells.append({
                    'bbs': entity['bbs'],
                    'type': entity['label']
                })
        
        rows = sorted(rows, key=lambda dct: dct['bbs'][1])
        cols = sorted(cols, key=lambda dct: dct['bbs'][0])
        grid = np.full((len(rows), len(cols)), fill_value=-1)
        # Autoincremental value of cell_id
        cell_id = 0
        def find_col_id(x):
            min_dist = sys.float_info.max
            col_id = 0
            for idx, col in enumerate(cols):
                if abs(col['bbs'][0] - x) < min_dist:
                    min_dist = abs(col['bbs'][0] - x)
                    col_id = idx
                if idx == len(cols) - 1:
                    if abs(col['bbs'][2] - x) < min_dist:
                        min_dist = abs(col['bbs'][2] - x)
                        col_id = idx + 1
            return col_id

        def find_row_id(y):
            min_dist = sys.float_info.max
            row_id = 0
            for idx, row in enumerate(rows):
                if abs(row['bbs'][1] - y) < min_dist:
                    min_dist = abs(row['bbs'][1] - y)
                    row_id = idx
                if idx == len(rows) - 1:
                    if abs(row['bbs'][3] - y) < min_dist:
                        min_dist = abs(row['bbs'][3] - y)
                        row_id = idx + 1
            return row_id

        cells_dict = {}
        for spanning_cell in spanning_cells:
            x1 = spanning_cell['bbs'][0]
            y1 = spanning_cell['bbs'][1]
            x2 = spanning_cell['bbs'][2]
            y2 = spanning_cell['bbs'][3]
            start_col_id = find_col_id(x1)
            end_col_id = find_col_id(x2)
            
            start_row_id = find_row_id(y1)
            end_row_id = find_row_id(y2)
            
            # print('col', start_col_id, end_col_id)
            # print('row', start_row_id, end_row_id)
            grid[start_row_id:end_row_id, start_col_id:end_col_id].fill(cell_id)
            cells_dict[cell_id] = {
                                   'start_row_id': start_row_id,
                                   'end_row_id': end_row_id,
                                   'start_col_id': start_col_id,
                                   'end_col_id': end_col_id
                                   }
            cell_id += 1
        
        for row_id in range(grid.shape[0]):
            for col_id in range(grid.shape[1]):
                if grid[row_id, col_id] == -1:
                    grid[row_id, col_id] = cell_id
                    cells_dict[cell_id] = {
                                   'start_row_id': row_id,
                                   'end_row_id': row_id + 1,
                                   'start_col_id': col_id,
                                   'end_col_id': col_id + 1
                                   }
                    cell_id += 1
        
        print('--------------------')
        print('GRID:')
        print(grid)
        print('--------------------')
        grid = grid.astype('str')
        for key in cells_dict:
            start_row_id = cells_dict[key]['start_row_id']
            start_col_id = cells_dict[key]['start_col_id']
            end_row_id = cells_dict[key]['end_row_id']
            end_col_id = cells_dict[key]['end_col_id']
            
            start_row_offset = rows[start_row_id]['bbs'][1]
            try:
                end_row_offset = rows[end_row_id]['bbs'][1]
            except:
                end_row_offset = rows[end_row_id-1]['bbs'][3]
                
            start_col_offset = cols[start_col_id]['bbs'][0]
            try:
                end_col_offset = cols[end_col_id]['bbs'][0]
            except:
                end_col_offset = cols[end_col_id-1]['bbs'][2]
            
            # canvas = np.full(((end_row_offset-start_row_offset)*3, (end_col_offset-start_col_offset)*3, 3), (255, 255, 255))
            # canvas = np.full_like(image, (255, 255, 255))
            # canvas[:end_row_offset-start_row_offset, :end_col_offset-start_col_offset, :] = image[start_row_offset:end_row_offset, start_col_offset: end_col_offset, :]
            cell_img = image[start_row_offset:end_row_offset, start_col_offset: end_col_offset, :]
            # cell_img = self.rescale_img(cell_img, 3)
            # cv2.imwrite(f'demo/cell_{key}.jpg', cell_img)
            # cell_img = cv2.cvtColor(cell_img, cv2.COLOR_BGR2GRAY)
            content = self.reader.readtext(cell_img, detail = 0)
            content = ' '.join(content)
            
            grid[start_row_id: end_row_id, start_col_id:end_col_id].fill('')
            grid[start_row_id, start_col_id] = content
            
        print('--------------------')
        print('GRID:')
        print(grid)
        print('--------------------')
        return grid


def get_class_map():
    class_map = {
        0: "table",
        1: "table column",
        2: "table row",
        3: "table column header",
        4: "table projected row header",
        5: "table spanning cell",
        6: "no object",
    }
    return class_map
    

def main():
    cmd_args = get_args().__dict__
    det_config_args = json.load(open(cmd_args['det_config_file'], 'rb'))
    for key, value in cmd_args.items():
        if not key in det_config_args or not value is None:
            det_config_args[key] = value
    #config_args.update(cmd_args)
    det_args = type('Args', (object,), det_config_args)
    
    rec_config_args = json.load(open(cmd_args['rec_config_file'], 'rb'))
    for key, value in cmd_args.items():
        if not key in rec_config_args or not value is None:
            rec_config_args[key] = value
    #config_args.update(cmd_args)
    rec_args = type('Args', (object,), rec_config_args)
    # print(det_args.__dict__)
    # print('-' * 100)

    print("loading model")
    device = torch.device(det_args.device)
    
    # Load detection model
    det_args.config_file = det_args.det_config_file
    det_args.model_load_path = det_args.det_model_load_path
    det_transformer = TableTransformer(det_args, device, get_class_map(), 'det')
    
    # Rec detection model
    rec_args.config_file = rec_args.rec_config_file
    rec_args.model_load_path = rec_args.rec_model_load_path
    rec_transformer = TableTransformer(rec_args, device, get_class_map(), 'rec')
    
    #Reader
    reader = easyocr.Reader(['vi','en'])
    
    # TableNet
    tablenet = TableNet('/home/ec2-user/table_transformer_khoi/tablenet/table_line_models/table-line.h5',
                        'easyocr',
                        reader,
                        (1024,1024))
    
    # Pipeline
    table_pipeline = TablePipeline(det_transformer, rec_transformer, tablenet, reader)
    
    if os.path.isdir(rec_args.image_path):
        table_pipeline.infer(rec_args)
    elif os.path.isfile(rec_args.image_path):
        table_pipeline.infer(rec_args)
        # table_pipeline.infer_bordered_table(rec_args)
    else:
        raise FileNotFoundError(f"file or folder {det_args.image_path} not found")


if __name__ == "__main__":
    main()