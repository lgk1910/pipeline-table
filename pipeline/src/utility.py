import argparse

def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--det_config_file',
                        required=True,
                        help="Filepath to the config containing the args")
    parser.add_argument('--rec_config_file',
                        required=True,
                        help="Filepath to the config containing the args")
    parser.add_argument('--backbone',
                        default='resnet18',
                        help="Backbone for the model")
    parser.add_argument('--image_path', help="Input image path")
    parser.add_argument('--thresh',
                        type=float,
                        default=0.5,
                        help="Threshold for the certainty of predictions")
    parser.add_argument('--output_dir', default='../output', type=str)
    parser.add_argument('--det_model_load_path', help="The path to trained model")
    parser.add_argument('--rec_model_load_path', help="The path to trained model")
    parser.add_argument('--load_weights_only', action='store_true')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--device')

    return parser.parse_args()

import sys

class BoundingBox:
    def __init__(self, 
                left, 
                top, 
                right, 
                bot,):
        self.left = left
        self.top = top
        self.right = right
        self.bot = bot
        self.area = abs((right-left)*(bot-top))

    def iou(self, bb: "BoundingBox"):
        x1 = max(self.left, bb.left)
        x2 = min(self.right, bb.right)
        y1 = max(self.top, bb.top)
        y2 = min(self.bot, bb.bot)
        intersection = abs((x2-x1)*(y2-y1))
        try:
            return intersection / (self.area + bb.area - intersection)
        except:
            return sys.float_info.max